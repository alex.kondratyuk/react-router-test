import React, {Component} from 'react';
import './style.css';
import fetchArticles from '../../api/fetchArticles'
import spinner from '../../assets/Spinner.svg';

class Articles extends Component {
    state = {
        listArticles: []
    };

    componentDidMount() {
        fetchArticles().then(res => {
            console.log(res);
            this.setState({
                listArticles: res.results
            });
            console.log('*****Articles*****componentDidMount');
            console.log(this.state.listArticles);
        });
    }

    render() {
        if (this.state.listArticles.length) {
            return (
                <div className='articles'>
                    <h2 className='articlesTitle'>Article page</h2>
                    <div className="articlesElements">
                        {
                            this.state.listArticles.map((item, index) => (
                                    <div key={index} className='articleItem'>
                                        <img src={item['title_images']['article_card']} alt="pics" className='articleItemImg'/>
                                        <h2 className='articleItemTitle'>{item.title}</h2>
                                    </div>
                                )
                            )
                        }
                    </div>

                </div>
            );
        } else {
            return (
                <div>
                    <img src={spinner} alt="spinner"/>
                </div>
            );
        }

    }
}

export default Articles