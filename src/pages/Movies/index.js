import React, {Component} from 'react';
import './style.css';
import fetchMovies from '../../api/fetchMovies';
import spinner from '../../assets/Spinner.svg'

class Movies extends Component {
    state = {
        list: [],
        imgWidth: 400
    };

    componentDidMount() {
        fetchMovies()
            .then(res => {
                this.setState({
                    list: res.results
                });
                console.log('*****AllFilms*****componentDidMount');
                console.log(this.state.list);
            });
    };

    render() {
        const imgPath = `https://image.tmdb.org/t/p/w${this.state.imgWidth}/`;
        if (this.state.list.length) {
            return (
                <div className='listMovies'>
                    {
                        this.state.list.map((item, index) => (
                            <div key={index}  className='movieBlock'>
                                <img src={`${imgPath}${item['poster_path']}`} alt="poster" className='movieItem'/>
                                <h3 className='movieItemTitle'>{item.title}</h3>
                                <h5 className='movieItemTitle'>Release date: {item['release_date']}</h5>
                            </div>

                        ))
                    }
                </div>
            );
        } else {
            return (
                <div>
                    <img src={spinner} alt="spinner"/>
                </div>
            );
        }

    }
}

export default Movies